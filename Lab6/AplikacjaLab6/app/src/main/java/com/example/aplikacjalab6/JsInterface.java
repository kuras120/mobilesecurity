package com.example.aplikacjalab6;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class JsInterface {
    Context context;

    public JsInterface(Context c) {
        this.context = c;
    }

    @JavascriptInterface
    public void makeToast(String text) {
        Toast.makeText(this.context, text, Toast.LENGTH_SHORT).show();
    }
}
