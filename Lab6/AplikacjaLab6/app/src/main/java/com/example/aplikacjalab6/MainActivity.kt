package com.example.aplikacjalab6

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.http.SslError
import android.os.Bundle
import android.os.IBinder
import android.os.Messenger
import android.provider.ContactsContract
import android.util.Log
import android.webkit.*
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


class MainActivity : AppCompatActivity() {


    companion object {
        const val TAG = "MainActivity";
        const val READ_CONTACTS_PERMISSION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var trustAllCerts: Array<TrustManager> = arrayOf<TrustManager>(
            object : X509TrustManager {
                override fun checkClientTrusted(
                    certs: Array<X509Certificate?>?, authType: String?
                ) {
                }
                override fun checkServerTrusted(
                    certs: Array<X509Certificate?>?, authType: String?
                ) {
                }
                override fun getAcceptedIssuers():
                        Array<X509Certificate> {
                    return emptyArray()
                }
            }
        )

        val sc = SSLContext.getInstance("SSL")
        sc.init(null, trustAllCerts, SecureRandom())
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)

        setContentView(R.layout.activity_main)

        val webView: WebView = findViewById(R.id.webView)

        webView.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler,
                er: SslError?
            ) {
                handler.proceed()
            }
        }
        webView.webChromeClient = WebChromeClient()
        webView.settings.javaScriptEnabled = true
        webView.loadUrl("https://google.com")
        webView.addJavascriptInterface(JsInterface(this), "AndroidFunction");

        findViewById<Button>(R.id.sendRequest).setOnClickListener { sendRequest() }
    }

    private fun sendRequest() {
        GlobalScope.launch {
            val url = URL("https://jsonplaceholder.typicode.com/posts")
            val con: HttpURLConnection = url.openConnection() as HttpURLConnection
            con.requestMethod = "GET"

            val inputStream = BufferedReader(
                InputStreamReader(con.inputStream)
            )
            var inputLine: String?
            val content = StringBuffer()
            while (inputStream.readLine().also { inputLine = it } != null) {
                content.append(inputLine)
            }
            inputStream.close()

            con.disconnect()

            Log.d(TAG, content.toString());

            GlobalScope.launch(Dispatchers.Main) {
                findViewById<TextView>(R.id.users).text = content.toString()
            }

        }
    }
}