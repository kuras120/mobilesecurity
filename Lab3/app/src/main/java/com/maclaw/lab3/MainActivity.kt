package com.maclaw.lab3

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private var READ_CONTACTS_PERMISSION_REQUEST_CODE: Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.ApiButton)
        button.setOnClickListener{
            Log.d("bagnolog", "test")
            thread {
                val connection = URL("https://jsonplaceholder.typicode.com/posts").openConnection() as HttpURLConnection
                try {
                    val data = connection.inputStream.bufferedReader().use { it.readText() }
                    val typeToken = object : TypeToken<List<User>>() {}.type
                    val users = Gson().fromJson<List<User>>(data,typeToken);
                    Log.d("bagnolog",users.toString());
                } finally {
                    connection.disconnect()
                }
            }
        }
        var kontaktButton = findViewById<Button>(R.id.KontaktyButton)
        kontaktButton.setOnClickListener {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.READ_CONTACTS), READ_CONTACTS_PERMISSION_REQUEST_CODE
            )
        }
        var mReceiver = object : NetworkReceiver() {}
        registerReceiver(mReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == READ_CONTACTS_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContacts()
            } else {
                Toast.makeText(this@MainActivity, "Nie można wyświetlić kontaktów - brak uprawnień", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("Range")
    fun getContacts() {
        val cursor = contentResolver.query( ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        )
        while (cursor!!.moveToNext()) {
            val contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
            val displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
            Log.d("bagnolog", "Contact ${contactId} ${displayName}")
        }
    }
}