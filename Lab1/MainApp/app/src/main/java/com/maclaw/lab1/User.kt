package com.maclaw.lab1

import android.content.Context
import android.database.Cursor
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

fun subscribeOnBackground(function: () -> Unit) {
    Single.fromCallable {
        function()
    }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe()
}

@Entity(tableName = "user_table")
data class User(val username: String,
                val session: Int,
                @PrimaryKey(autoGenerate = true) val id: Int? = null)

@Dao
interface UserDao {

    @Insert
    fun insert(user: User)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)

    @Query("delete from user_table")
    fun deleteAllUsers()

    @Query("select * from user_table order by id desc")
    fun getAllUsers(): LiveData<List<User>>

    @Query("SELECT COUNT(id) FROM user_table")
    fun getCount(): Int

    @Query("SELECT * FROM user_table")
    fun findAll(): Cursor?
}

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        private var instance: UserDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): UserDatabase {
            if(instance == null)
                instance = Room.databaseBuilder(ctx.applicationContext, UserDatabase::class.java,
                    "user_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .allowMainThreadQueries()
                    .build()

            return instance!!

        }

        private val roomCallback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                Log.d("essa","odpalono bazę danych!")
                super.onCreate(db)
                //populateDatabase(instance!!)
            }
        }
    }
}