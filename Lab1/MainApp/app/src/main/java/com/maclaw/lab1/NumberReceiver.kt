package com.maclaw.lab1

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.util.Log
import android.widget.Toast

open class NumberReceiver: BroadcastReceiver() {

    private var database: UserDatabase? = null

    override fun onReceive(p0: Context, p1: Intent?) {
        database = UserDatabase.getInstance(p0)
        p1?.let{
            registerUser(p1.getStringExtra("username").toString(),p1.getIntExtra("time",0).toInt())
        }
    }

    fun registerUser(username: String, time: Int) {
        //if(database === null) Log.d("essa","brak bazy danych!")
        subscribeOnBackground {
            database?.userDao()?.insert(User(username,time))
            Log.d("NumberReceiver", "zarejestrowano uzytkownika " + username + " i czas " + time);
        }
    }

    fun count(cx: Context) {
        Toast.makeText(cx, "Test", Toast.LENGTH_SHORT).show();
        if(database === null) database = UserDatabase.getInstance(cx)
        subscribeOnBackground {
            //Log.d("essa","Ilosc wpisow: " + database?.userDao()?.getCount())
            //Toast.makeText(cx, "Wierszy: " + , Toast.LENGTH_SHORT).show();
            var c = database?.userDao()?.findAll()
            if (c!!.moveToFirst()) {
                do {
                    Log.d("essa","Uzytkownik: " + c.getString(0) + " | Ilość: " + c.getInt(1))
                } while (c!!.moveToNext());
            }
        }
    }

}