package com.maclaw.lab1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class UserActivity : AppCompatActivity() {
    private var mNumberReceiver: NumberReceiver? = null
    private var username: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        mNumberReceiver = object : NumberReceiver() {}

        registerReceiver(mNumberReceiver as NumberReceiver, IntentFilter("com.maclaw.UZYTKOWNIK"));

        username = intent.getStringExtra("name").toString();
        val name = findViewById<TextView>(R.id.user_name)
        name.text = username;

        val startBtn = findViewById<Button>(R.id.start_service);
        var killBtn = findViewById<Button>(R.id.kill_services)
        var countBtn = findViewById<Button>(R.id.count_db);
        startBtn.setOnClickListener{
            startService(Intent(applicationContext, CountdownService::class.java).putExtra("username",username))
        }
        killBtn.setOnClickListener{
            stopServices();
        }
        countBtn.setOnClickListener {
            mNumberReceiver?.count(this)
        }
    }

    private fun stopServices() {
        stopService(Intent(applicationContext, CountdownService::class.java))
    }

    override fun onDestroy() {
        mNumberReceiver?.let {
            unregisterReceiver(it)
        }
        stopServices();
        super.onDestroy()
    }
}