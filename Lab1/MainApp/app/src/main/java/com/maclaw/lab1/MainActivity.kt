package com.maclaw.lab1

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.maclaw.lab1.MyContentProvider

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button_first)
        var text = findViewById<EditText>(R.id.name_edit)
        button.setOnClickListener{
            val txt = text.text.toString();
            if(txt.length>0) {
                val intent = Intent(this, UserActivity::class.java)
                intent.putExtra("name",txt)
                startActivity(intent)
            } else {
                Toast.makeText(this@MainActivity, "Musisz podać nazwę użytkownika!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}