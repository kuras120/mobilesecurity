package com.maclaw.lab1

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.Message
import android.util.Log
import android.widget.Toast
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import java.lang.Exception
import java.util.*


class CountdownService : Service() {
    private val timer = Timer()
    private var username: String? = null
    private var ctx: Context? = null
    private var threads = mutableListOf<Thread>()

    override fun onBind(arg0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        ctx = this
        intent?.let {
            username = intent.getStringExtra("username").toString()
        }
        startService()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun startService() {
        threads.add(object: Thread() {
            var time = 0
            override fun run() {
                Looper.prepare();
                while(true){
                    time++;
                        Log.d("Timer","Minelo sekund: " + time);
                    try{
                        Thread.sleep(1000);
                    }catch(e: Exception){
                        var intent = Intent("com.maclaw.UZYTKOWNIK");
                        intent.putExtra("username",username as String)
                        intent.putExtra("time",time)
                        sendBroadcast(intent)
                        return
                    }
                }
            }
        })
        threads.last().start()
    }
    override fun onDestroy() {
        for(thread in threads) {
            thread.interrupt();
        }
        super.onDestroy()
    }

}