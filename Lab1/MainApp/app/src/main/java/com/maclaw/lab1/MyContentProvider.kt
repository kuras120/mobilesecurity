package com.maclaw.lab1

import android.content.ContentValues

import android.content.ContentUris

import android.content.UriMatcher

import android.content.ContentProvider
import android.database.Cursor
import android.net.Uri
import androidx.annotation.Nullable
import java.lang.IllegalArgumentException


class MyContentProvider : ContentProvider() {
    private var userDao: UserDao? = null

    companion object {

        const val PROVIDER_NAME = "com.maclaw.lab1.MyContentProvider"
        const val uriCode = 1
        const val ID_PERSON_DATA = 1

        val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            uriMatcher.addURI(PROVIDER_NAME, "users", uriCode);
        }
    }

    override fun onCreate(): Boolean {
        userDao = context?.let {
            UserDatabase.getInstance(it).userDao()
        }
        return false
    }

    @Nullable
    override fun query(
        uri: Uri, projection: Array<String>?, selection: String?,
        selectionArgs: Array<String>?, sortOrder: String?
    ): Cursor? {
        val cursor: Cursor
        when (uriMatcher.match(uri)) {
            ID_PERSON_DATA -> {
                cursor = userDao!!.findAll()!!
                if (context != null) {
                    cursor.setNotificationUri(
                        context!!
                            .getContentResolver(), uri
                    )
                    return cursor
                }
                throw IllegalArgumentException("Unknown URI: $uri")
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun getType(uri: Uri): String? {
        return when (uriMatcher!!.match(uri)) {
            uriCode -> "vnd.android.cursor.dir/users"
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
    }

    override fun insert(p0: Uri, p1: ContentValues?): Uri? {
        TODO("Not yet implemented")
    }

    override fun delete(p0: Uri, p1: String?, p2: Array<out String>?): Int {
        TODO("Not yet implemented")
    }

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<out String>?): Int {
        TODO("Not yet implemented")
    }

}