package com.woku.readerapp

import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.ContentProviderClient
import android.util.Log


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        try {
            val URL = "content://com.maclaw.lab1.MyContentProvider/users"
            val data = Uri.parse(URL)
            val CR = contentResolver.acquireContentProviderClient(data)
            val cursor: Cursor? = CR?.query(data, null, null, null, null)
                if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        Log.d(
                            "ContentResolver",
                            "Użytkownik: " +
                                    cursor.getString(cursor.getColumnIndex("username")).toString() +
                                    " | Ilość: " +
                                    cursor.getInt(cursor.getColumnIndex("session")).toString()
                        )
                    } while (cursor.moveToNext())
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}